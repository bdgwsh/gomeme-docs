
<div style="text-align:center"><img src="./images/gomeme_logo.png" alt="logo" /></div>
<h1 align='center'>GoMeme-API</h1>

Service puts your text on an image. Create [image macro](https://en.wikipedia.org/wiki/Image_macro) style memes.
Based on [nomad-software/meme](https://github.com/nomad-software/meme).

## Usage example
![](https://i.imgur.com/hBK28bf.gif)

## Installation
* Installation via Go
* Run go get -u -v gitlab.com/NikkoFox/gomeme-api

## Docker
Docker image is available in the project [Container Registry](https://gitlab.com/NikkoFox/gomeme-api/container_registry)
```bash
docker login registry.gitlab.com
docker run -p "7744:7744" -d registry.gitlab.com/nikkofox/gomeme-api/develop
```

--------

## Details description

* [Draw text using HTTP POST via form-data](#post-image-by-form)
* [Draw text using HTTP POST via JSON](#post-image-by-json)
* [Responses](#response)

--------

### Draw text using HTTP POST via form-data


***Endpoint:***

```bash
Method: POST
Type: FORMDATA
URL: /meme/create
```

***Body:***

| Key | Type | Description |
|---|------|-------------|
| image | file form-data (Required) |  |
| top_text | string (Optional) |  |
| bottom_text | string (Optional) |  |
| fast_stroke | string (Optional, default: false) | Convert to bool. Inaccurate but fast stroke |

` $ curl --location --request POST 'localhost:7744/meme/create' \
--form 'image=@"./your_picture_path.jpg"' \
--form 'top_text="test!"'`

### Draw text using HTTP POST via JSON


***Endpoint:***

```bash
Method: POST
Type: JSON
URL: /meme/create
```



***Body:***

| Key | Type | Description |
| --- | ------|-------------|
| image | string (Required) | URL \| image path on server \| [Data URI](https://en.wikipedia.org/wiki/Data_URI_scheme) Base64 Encoded Image |
| top_text | string (Optional) |  |
| bottom_text | string (Optional) |  |
| fast_stroke | bool (Optional, default: false) | Inaccurate but fast stroke |


```js        
{
    "image": "https://via.placeholder.com/600", 
    "top_text": "sample top text",
    "bottom_text": "sample bottom text"
}
```

### Response

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | image/jpeg | Binary data stream for the responsive image. |