# Adding custom fonts

It is possible to add any font you want by copying the font into the `fonts` folder and passing `FONT` environment variable.
In order to add your custom fonts follow the steps:

1.` $ git clone https://gitlab.com/NikkoFox/gomeme-api`

2.`$ cp <your_font_path> data/fonts`

3.` $ docker build -t gomeme:latest . `

4.` $ docker run -p "7744:7744" -e FONT="<your_font_name>" gomeme:latest`

Animated example is shown below.

![Example](./images/gomeme-custom-fonts.gif)
